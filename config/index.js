export default {
   env : process.env.NODE_ENV || 'development',
   port : process.env.PORT || 8000,
   API_URL: "http://upcalibrebackend.herokuapp.com"
}
