import React, { Component } from "react";
import { Stage, Layer, Rect, Line } from "react-konva";
import GridCell from "./GridCell";

class Home extends Component {
  GRID = 100;
  state = {
    GRID_WIDTH: 1500,
    GRID_WIDTH_X: 2000,
    GRID_WIDTH_Y: 1000,
    LINES_A: [],
    LINES_B: [],
    Rectangels: []
  };

  loadRectanlges = async range => {
    console.log("API Call started !");
    const response = await fetch(
      `http://upcalibrebackend.herokuapp.com/api/poc/grid/${range}/${range}`
    );
    const data = await response.json();
    console.log("API Call End !");
    return data;
  };

  drawGridLines() {
    const linesA = [],
      linesB = [];
    const loadedBlocks = parseInt(this.state.GRID_WIDTH / this.GRID);
    for (let i = 0; i <= this.state.GRID_WIDTH / this.GRID; i++) {
      linesA.push(
        <Line
          strokeWidth={1}
          stroke={"#c7c7c7"}
          points={[i * this.GRID, 0, i * this.GRID, this.state.GRID_WIDTH]}
        />
      );

      linesB.push(
        <Line
          strokeWidth={1}
          stroke={"#c7c7c7"}
          points={[0, i * this.GRID, this.state.GRID_WIDTH, i * this.GRID]}
        />
      );
    }

    this.loadRectanlges(loadedBlocks).then(rects => {
      this.setState(preState => ({
        LINES_A: [...preState.LINES_A, ...linesA],
        LINES_B: [...preState.LINES_B, ...linesB],
        Rectangels: [...preState.Rectangels, ...rects]
      }));
    });
  }

  componentDidMount() {
    this.drawGridLines();
    window.addEventListener("scroll", this.getXScroll);
    window.addEventListener("scroll", this.getYScroll);
  }

  componentDidUpdate() {
    window.addEventListener("scroll", this.getXScroll);
    window.addEventListener("scroll", this.getYScroll);
  }

  loadMoreItems(axis) {
    if (axis == "x") {
      this.setState(preState => ({
        GRID_WIDTH: parseInt(preState.GRID_WIDTH) + 1000,
        GRID_WIDTH_X: parseInt(preState.GRID_WIDTH_X) + 1000
      }));
    } else {
      this.setState(preState => ({
        GRID_WIDTH: parseInt(preState.GRID_WIDTH) + 1000,
        GRID_WIDTH_Y: parseInt(preState.GRID_WIDTH_Y) + 1000
      }));
    }
    this.drawGridLines();
  }

  getYScroll = () => {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
      console.log("y scroll");
      window.removeEventListener("scroll", this.getScrollElement);
      this.state.GRID_WIDTH_Y <= 15000 && this.loadMoreItems("y");
    }
  };

  getXScroll = () => {
    if (window.innerWidth + window.scrollX >= document.body.offsetWidth) {
      window.removeEventListener("scroll", this.getScrollElement);
      this.state.GRID_WIDTH_X <= 15000 && this.loadMoreItems("x");
    }
  };

  render() {
    return (
      <Stage width={this.state.GRID_WIDTH_X} height={this.state.GRID_WIDTH_Y}>
        <Layer>
          {this.state.LINES_A}
          {this.state.LINES_B}
        </Layer>
        <Layer>
          <GridCell GRID={this.GRID} RECT={this.state.Rectangels} />
        </Layer>
      </Stage>
    );
  }
}

export default Home;
