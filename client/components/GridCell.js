import React, { Fragment, Component } from "react";
import { Rect, Group, Text } from "react-konva";

class GridCell extends Component {
  addRectangelsToGrid(Rectangels) {
    const finalArray = [];
    Rectangels.forEach(rect => {
      const {
        name,
        color,
        values: {
          id,
          position: { x, y }
        }
      } = rect;
      finalArray.push(
        <Group
          x={Math.round(x) * this.props.GRID}
          y={Math.round(y) * this.props.GRID}
          width={this.props.GRID}
          height={this.props.GRID}
          onDragEnd={e => {
            e.target.to({
              x: Math.round(e.target.x() / this.props.GRID) * this.props.GRID,
              y: Math.round(e.target.y() / this.props.GRID) * this.props.GRID
            });
          }}
          draggable={true}
        >
          <Rect width={this.props.GRID} height={this.props.GRID} fill={color} />
          <Text
            text={id}
            fontSize={18}
            fontFamily="Calibri"
            x={20}
            y={10}
            padding={5}
            verticalAlign="top"
            align="center"
          />
          <Text x={40} y={35} text="+" fontSize={30} fill="black" />
          <Text
            text={name}
            fontSize={16}
            fontFamily="Calibri"
            x={5}
            y={60}
            verticalAlign="bottom"
            align="center"
          />
        </Group>
      );
    });
    return finalArray;
  }

  render() {
    return <Fragment>{this.addRectangelsToGrid(this.props.RECT)}</Fragment>;
  }
}
export default GridCell;
